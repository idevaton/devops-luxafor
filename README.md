# devops luxafor with

This is an example project for working with the Luxafor flag. The goal for this project is to showcase a working code example for changing flag color based on events, such as creating new issues in JIRA or changes in the state of Azure Subscription. This project is one of many examples of working with the flag. Feel free to explore your use cases.

# available code bases

 - ASP DotNet MVC
 - ASP Core MVC
 - DotNet Console App
 - Wordpess Plugin PHP
 - Drupal Module PHP (coming soon)
 - Docker Service + Node.JS (coming soon)

# possible use cases

 - checking Azure Subscription state
 - website alerts based on specific events
 - ticket alerts for JIRA
 - database events

# ASP DotNet MVC - getting started
 - Using Visual Studio Open \aspnet\aspnet.sln
 - To change flag colors, start the app and provide flag and JIRA parameters

 # DotNet Console App - getting started
 - Using Visual Studio Open \consoleapp\azurecanary\azurecanary.sln
 - To change flag colors, start the app and provide flag and Azure parameters

 ### Azure Scopes:
 - api://xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/.default - your own scope in the API of an application
 - https://graph.microsoft.com/.default
 - https://management.azure.com/.default
 - https://management.core.windows.net/.default

# Wordpress - getting started
 - Download plugin and template files located in \wordpress directory
 - Install and enable plugin Luxafor in the Wordpress site /wp-content/plugins/luxafor
 - Place PHP template template-luxafor.php into site's active theme folder: /wp-content/themes/twentytwentyone/page-templates/template-luxafor.php
 - Create a page called Luxafor, pick Luxafor as a page template and set page URL to /index.php/luxafor-page/
 - Create a post or use one of the existing posts and place a button in the post's body linking to /index.php/luxafor-page/ page
 - Test to see if the link works and Luxafor page opens without any issues
 - Create another new page and call it Jira, in the body set short code to [jira], set URL to /index.php/jira/
 - Create another new page and call it Send Color, in the body set short code to [sendcolor], set URL to /index.php/send-color/
 - Test the links from /index.php/luxafor-page/ page to make sure links to Jira and Send Color pages work
 - To change flag colors, provide flag and JIRA parameters

# disclaimer

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
