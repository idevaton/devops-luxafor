﻿using aspcore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace aspcore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult SendColor()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendColor(Models.LuxaforFlag model)
        {
            if (model != null && model.LuxaforFlagId != null && model.Color != null)
            {
                Models.LuxaforFlagResult result = Classes.AppFunctions.CallLuxaforFlag(model.LuxaforFlagId, model.Color);
                if (result != null && result.Result)
                {
                    TempData["success"] = result.Message;
                }
                else
                {
                    TempData["error"] = result.Message;
                }
            }
            return View(model);
        }

        public ActionResult Jira()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Jira(Models.Jira model)
        {
            if (model != null && model.APIToken != null && model.LuxaforFlagId != null && model.OrgName != null && model.UserName != null)
            {
                Models.LuxaforFlagResult result = Classes.AppFunctions.CheckJira(model.LuxaforFlagId, model.OrgName, model.APIToken, model.UserName);
                if (result != null && result.Result)
                {
                    TempData["success"] = result.Message;
                }
                else
                {
                    TempData["error"] = result.Message;
                }
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult JiraJson(string jiraOrgName, string jiraUserName, string jiraApiToken, string luxaforFlagId)
        {
            if (jiraOrgName != null && jiraUserName != null && jiraApiToken != null && luxaforFlagId != null)
            {
                Models.LuxaforFlagResult result = Classes.AppFunctions.CheckJira(luxaforFlagId, jiraOrgName, jiraApiToken, jiraUserName);
                if (result != null && result.Result)
                {
                    TempData["success"] = result.Message;
                }
                else
                {
                    TempData["error"] = result.Message;
                }
            }
            return Json(new { orgName = jiraOrgName, userName = jiraUserName, apiToken = jiraApiToken });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
