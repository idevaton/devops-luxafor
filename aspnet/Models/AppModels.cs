﻿using System.ComponentModel.DataAnnotations;

namespace aspnet.Models
{
    //Model for Luxafor Flag
    public class LuxaforFlag
    {
        [Required]
        public string LuxaforFlagId { get; set; }
        [Required]
        public string Color { get; set; }
    }

    public class Jira
    {
        [Required]
        public string OrgName { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string APIToken { get; set; }
        [Required]
        public string LuxaforFlagId { get; set; }
    }

    public class JiraTicket
    {
        [Required]
        public string key { get; set; }
    }

    public class LuxaforFlagResult
    {
        public bool Result { get; set; }
        public string Message { get; set; }
    }
}