<?php /* Template Name: Luxafor */

    $colorSent = false;
    $orgName = get_query_var('OrgName');
    $userName = get_query_var('UserName');
    $apiToken = get_query_var('APIToken');
    $luxaforFlagId = get_query_var('LuxaforFlagId');

    if(!empty($orgName) && !empty($userName) && !empty($apiToken) && !empty($luxaforFlagId))
    {
        $colorSent = true;
        luxafor_check_jira($orgName, $userName, $apiToken, $luxaforFlagId);
    }
?>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<?php if($colorSent) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success alert-dismissible">
                <p>Jira Checked!</p>
            </div>
        </div>
    </div>
<?php } ?>

<div class="jumbotron">
    <form id="luxafor-send-color" method="GET" role="tabpanel" autocomplete="off">
        <div class="row">    
            <h2>Get Open JIRA Issues Manually</h2>
            <div class="col-md-4">
                <h3>Jira Org Name</h3>
                <input class="form-control" data-val="true" data-val-required="The OrgName field is required." id="OrgName" name="OrgName" type="text" value="<?= $orgName ?>" />
                <span class="field-validation-valid text-danger" data-valmsg-for="OrgName" data-valmsg-replace="true"></span>
            </div>
            <div class="col-md-4">
                    <h3>Jira User Name</h3>
                    <input class="form-control" data-val="true" data-val-required="The UserName field is required." id="UserName" name="UserName" type="text" value="<?= $userName ?>" />
                    <span class="field-validation-valid text-danger" data-valmsg-for="UserName" data-valmsg-replace="true"></span>
            </div>
            <div class="col-md-4">
                <h3>Jira API Token</h3>
                <input class="form-control" data-val="true" data-val-required="The APIToken field is required." id="APIToken" name="APIToken" type="text" value="<?= $apiToken ?>" />
                <span class="field-validation-valid text-danger" data-valmsg-for="APIToken" data-valmsg-replace="true"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>Luxafor Webhook Id</h3>
                <input class="form-control" data-val="true" data-val-required="The LuxaforFlagId field is required." id="LuxaforFlagId" name="LuxaforFlagId" type="text" value="<?= $luxaforFlagId ?>" />
                <span class="field-validation-valid text-danger" data-valmsg-for="LuxaforFlagId" data-valmsg-replace="true"></span>
                <p>Note: the color of the flag will go Red if you have any new issues in JIRA</p>
            </div>
        </div>
        <div class="row" style="padding-top: 15px;">
            <div class="col-md-12">
                <p><input type="submit" value="Check Jira" class="btn btn-warning" /></p>
                <p class="bg-warning p-2">Note: it may take few seconds to get response and for the color of the flag to change. If not, try to switch to Webhook tab in the Luxafor Desktop Application, or restart all services.</p>
            </div>
        </div>
    </form>

    <h2>Or Start Checking Continually</h2>
    <div class="row" style="padding-top: 15px;">
        <div class="col-md-12">
            <p>
                <input type="button" id="MyButton" class="btn btn-success" value="Start Jira Monitor">
                <a href="/index.php/jira" class="btn btn-danger">Stop Monitor</a>
            </p>
        </div>
    </div>
    <div class="row logarea" style="padding-top: 15px; display: none">
        <div class="col-md-12">
            <h3>Jira Log</h3>
            <div class="log"></div>
        </div>
    </div>
</div>

<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous">
</script>

<script>
    "use strict";
    jQuery(document).ready(function () {
        $('#MyButton').click(function () {
            var OrgName = $('#OrgName').val();
            var UserName = $('#UserName').val();
            var APIToken = $('#APIToken').val();
            var LuxaforFlagId = $('#LuxaforFlagId').val();
            
            if (OrgName != '' && UserName != '' && APIToken != '' && LuxaforFlagId != '') {
                $('.logarea').show();
                myLoop(OrgName, UserName, APIToken, LuxaforFlagId);
            }
            else {
                alert('Please fill out all the form fields');
            }
        });
    });

    var i = 1;                  //  set your counter to 1
    function myLoop(OrgNameVal, UserNameVal, APITokenVal, LuxaforFlagId) {         //  create a loop function
        setTimeout(function () {   //  call a 3s setTimeout when the loop is called
            //check if Jira has issues
            
            $.ajax({
                url: '<?php echo plugin_dir_url( __FILE__ ) . 'jiracheck.php'; ?>'
                , type: "POST"
                , dataType: "json"
                , data: { jiraOrgName: OrgNameVal, jiraUserName: UserNameVal, jiraApiToken: APITokenVal, luxaforFlagId: LuxaforFlagId },
                success: function (data) {
                    $(".log").append("<p>Loop: " + i + " | Org: " + data.orgName + " | User: " + data.userName + " | Token: " + data.apiToken + " | FlagId: " + data.luxaforFlagId + " | Result: " + data.result + "</p>");
                },
                error: function(jqXHR, exception) {
                    if (jqXHR.status === 0) {
                        alert('Not connect.\n Verify Network.');
                    } else if (jqXHR.status == 404) {
                        alert('Requested page not found. [404]');
                    } else if (jqXHR.status == 500) {
                        alert('Internal Server Error [500].');
                    } else if (exception === 'parsererror') {
                        alert('Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        alert('Time out error.');
                    } else if (exception === 'abort') {
                        alert('Ajax request aborted.');
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText);
                    }
                }
            });

            i++;                    //  increment the counter
            if (i < 1000)
            {           //  if the counter < 1000, call the loop function
                myLoop(OrgNameVal, UserNameVal, APITokenVal, LuxaforFlagId);             //  ..  again which will trigger another
            }
        }, 3000) //  ..  setTimeout()
    }
</script>