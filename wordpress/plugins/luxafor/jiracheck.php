<?php 

$jiraOrgName = $_POST['jiraOrgName'];
$jiraUserName = $_POST['jiraUserName'];
$jiraApiToken = $_POST['jiraApiToken'];
$luxaforFlagId = $_POST['luxaforFlagId'];

$arr = array('orgName' => 'n/a', 'userName' => 'n/a', 'apiToken' => 'n/a', 'luxaforFlagId' => 'n/a', 'result' => 'n/a');

if(!empty($jiraOrgName) && !empty($jiraUserName) && !empty($jiraApiToken) && !empty($luxaforFlagId))
{
    $result = 'Jira Check Called';
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://' . $jiraOrgName . '.atlassian.net/rest/api/2/search?jql=project=LT%20AND%20status=%22New%22&fields=status',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json',
          'Accept: application/json',
          'Authorization: Basic '. base64_encode($jiraUserName . ':' . $jiraApiToken)
        ),
    ));

    $response = curl_exec($curl);
    $obj = json_decode($response);

    if (empty($obj->{'issues'})) {
        // No open issues, set the light to green

        $curl2 = curl_init();

        curl_setopt_array($curl2, array(
            CURLOPT_URL => 'https://api.luxafor.com/webhook/v1/actions/solid_color',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{ "userId": "' . $luxaforFlagId . '", "actionFields":{ "color": "green" }}',
            CURLOPT_HTTPHEADER => array(
              'Content-Type: application/json'
            ),
          ));

        $jsonresponse = curl_exec($curl2);

        curl_close($curl2);

        $result = 'No open issues, set the light to green';
    }else{
        // Open issues present, set the light to red

        $curl2 = curl_init();

        curl_setopt_array($curl2, array(
            CURLOPT_URL => 'https://api.luxafor.com/webhook/v1/actions/solid_color',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{ "userId": "' . $luxaforFlagId . '", "actionFields":{ "color": "red" }}',
            CURLOPT_HTTPHEADER => array(
              'Content-Type: application/json'
            ),
          ));
        
        $jsonresponse = curl_exec($curl2);

        curl_close($curl2);

        $result = 'Open issues present, set the light to red';
    }

    curl_close($curl);

    $arr = array('orgName' => $jiraOrgName, 'userName' => $jiraUserName, 'apiToken' => $jiraApiToken, 'luxaforFlagId' => $luxaforFlagId, 'result' => $result);
}
echo json_encode($arr);

?>